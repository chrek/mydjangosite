from django.http import Http404
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.template import loader

from .models import Question

""" Using template namespace==========================
    def index(request):
    #return HttpResponse("Hello, world. You're at the polls index.")
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # output = ', '.join([q.question_text for q in latest_question_list])
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return HttpResponse(template.render(context, request)) 
    ===== End of Using template namespace=========================="""

# Using shortcut: render
def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {
        'latest_question_list': latest_question_list,
    }
    return render(request, 'polls/index.html', context)
# End of Using shortcut: render
""" def detail(request, question_id):
    return HttpResponse("This is question %s." % question_id) """
# Raising a 404 error
""" def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'polls/detail.html', {'question': question}) """
# End of Raising a 404 error
# Using shortcut: get_object_or_404()
def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})    
# End of using shortcut: get_object_or_404()
def results(request, question_id):
    response = "These are the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("This is your vote for question %s." % question_id)
